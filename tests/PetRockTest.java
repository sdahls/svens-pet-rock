import org.junit.Before;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class PetRockTest {
    private PetRock rocky;

    @BeforeEach
    public void myTestSetup(){
            rocky = new PetRock("Rocky");
    }



    @Test
    public void getName() {

        assertEquals("Rocky", rocky.getName());
    }

    @Test
    public void testUnhappyToStart() {

        assertFalse(rocky.isHappy());
    }

    @Test
    public void testHappyAfterPlay() {
        rocky.playWithRock();
        assertTrue(rocky.isHappy());
    }

    @Disabled("Exepting throwing not defined")
    @Test //(expected = IllegalStateException.class)
    public void nameFail() throws Exception{
      rocky.getHappyMessage();
    }

    @Test
    public void name() throws Exception{
        rocky.playWithRock();
        String msg= rocky.getHappyMessage();
        assertEquals("Im happy!", msg);
    }

    @Test
    public void testFavNum() {
        assertEquals(42 ,rocky.getFavNumber());
    }

    @Test
    void emptyNameFail() throws Exception {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    PetRock woofy = new PetRock("");
                });
    }



}